﻿using System.Text.Json.Serialization;

namespace KamiSama.Extensions.Json
{
	/// <summary>
	/// Json Discriminator Attribute
	/// </summary>
	public class JsonHirerarchyDiscriminatorAttribute : JsonAttribute
	{
	}
}

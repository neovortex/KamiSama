﻿global using KamiSama.Extensions.Core.Tests.Properties;
global using KamiSama.Extensions.Paging;
global using Microsoft.VisualStudio.TestTools.UnitTesting;
global using System;
global using System.Collections.Generic;
global using System.Globalization;
global using System.IO;
global using System.Linq;
global using System.Security.Cryptography;
global using System.Threading;
global using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;

[assembly: ExcludeFromCodeCoverage]
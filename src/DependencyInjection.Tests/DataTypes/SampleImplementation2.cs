﻿using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Extensions.DependencyInjection.Tests.DataTypes
{
	[ExcludeFromCodeCoverage]
	public class SampleImplementation2 : ISampleInteface
	{
		public int X { get; set; }
	}
}
